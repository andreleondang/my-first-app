import React, { Component } from 'react'
import TarjetaView from '../views/tarjeta.view'

class TarjetaAction extends Component{
  state = {
    cobro : 0
  }

  /**
   * @author André
   * @description Se utiliza para cobrar 222
   * @memberof TarjetaAction
   */
  cobrar = () =>{
    let pago = this.props.edad * 222
    this.setState( {cobro : pago } )
  }

  render (){
    console.log(this.props.img)
     return(
        <TarjetaView
        nonbre={this.props.nombre}
        edad={this.props.edad}
        img={this.props.img}
        pago={this.state.cobro}
        cobrar={this.cobrar} />
      )
  }
}

export { TarjetaAction as default }