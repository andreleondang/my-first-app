import React from 'react'
import {Card, Button } from '@material-ui/core';

const TarjetaView = (props) => {
  console.log(props.img)
  return(
    <Card style={{ height:'auto', width:'500px', margin:'auto', marginTop:'40px' }} >
      <img style={{ marginTop:'20px' }} alt='El Salsas' height='300px' width='300px' src={props.img}></img>
      <p> El pago será {props.pago} </p>
      <Button color="primary" onClick={ () => {
        props.cobrar()
      } } >Cobrar</Button>
    </Card>
  ) 
    
}

export { TarjetaView as default }